﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NAVI : MonoBehaviour
{
    // Start is called before the first frame update

    //public variables that use the transform component to start and end movement
    public Transform start;
    public Transform end;
    //public float that indicates start time, duration and end of movement
    public float startTime;
    public float duration;
    private float finished;

    void Start()
    {
        // uses Lines script to create Arrow
        NS.Lines.Make("path").Arrow(start.position, end.position, Color.green);
        // finishes movement by adding start time and duration together
        finished = startTime + duration;
    }

    // Update is called once per frame
    void Update()
    {
        // Uses Lines script to transform Quaternion (3D rotation)
        NS.Lines.Make("Q").Quaternion(transform.rotation, Color.yellow);
        if(Time.time < startTime)
        {
           // return;
        }


        Vector3 delta = end.position - start.position;
        float t = Time.time - startTime;
        float progress = t / duration;
        Vector3 distanceTraveled = progress * delta;
        NS.Lines.Make("progress").Line(Vector3.up * progress, Color.red);
        NS.Lines.Make("d").Arrow(distanceTraveled, Color.cyan);
        if(Time.time >= finished)
        {
           // enabled = false;
        }
      //  Vector3 target = start.position + distanceTraveled;
      //  transform.position = target;

        transform.position = Vector3.Lerp(start.position, end.position, progress);


    }
}
