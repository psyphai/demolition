﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using SonicBloom;

public class RythmverseGameManager : MonoBehaviour
{
	public static RythmverseGameManager Instance;
	
	public SonicBloom.Koreo.IKoreographedPlayer player;

	public int targetsMax = 0;
	public int targetsReached = 0;
	public int targetsPassed = 0;
	public int targetsCleared = 0;
	public int targetsMissed = 0;
	public int targetsHit = 0;
	public float ratioCleared = 1f;
	public int percentCleared = 0;
	public int streak = 0;
	public int longestStreak = 0;
	
	public int health = 100;
	public int healthMax = 100;
	
	public string songTitle = "";
	public string songAlbum = "";
	public string songArtist = "";
	public float songDuration = 0f;
	public float songElapsed = 0f;
	public Difficulty songDifficulty = Difficulty.Medium;
	
	public class Song{
		public string songTitle = "";
		public string songAlbum = "";
		public string songArtist = "";
		public float songDuration = 0f;
		public float songElapsed = 0f;
		public Difficulty songDifficulty = Difficulty.Medium;
		public Texture2D songArt;
		public AudioClip audioClip;
	}
	
	public Song song;
	public List<Song> songs;
	
	public bool countingDown = false;
	public bool playing = false;
	public float playingStartTime;
	public float playingElapsed;
	public bool paused = false;
	
	public enum Difficulty
	{
		Easy,
		Medium,
		Hard
	}
	
	public GameObject failGO;
	public GameObject winGO;
	public TMP_Text maxText;
	public TMP_Text clearedText;
	public TMP_Text streakText;
	public TMP_Text statsText;
	public void Register()
	{
		targetsMax++;
	}
	
	public void Clear()
	{
		targetsCleared++;
	}
	
	public void Miss()
	{
		targetsMissed++;
		streak = 0;
		health--;
		if (health<1)
		{
			Fail();
		}
	}
	
	public void Streak()
	{
		streak++;
	}
	
	public void Fail()
	{
		
	}
	
	#region MonoBehavior methods
	void Awake()
	{
		Instance=this;
	}
	
	void ResetStats()
	{
		targetsMax = 0;
		targetsCleared = 0;
		targetsMissed = 0;
		targetsHit = 0;
		streak = 0;
		longestStreak = 0;
		health = 100;
		healthMax = 100;
	}
	
	void UpdateStats()
	{
		if (statsText != null)
		{
			percentCleared = (int)ratioCleared * 100;
			statsText.text = "STATS:\n"+
				"CLEARED: " + percentCleared + "% "+targetsCleared+" / " + targetsMax + "\n" +
				"MISSED: " + targetsMissed + "\n" +
				"STREAK: " + streak + "\n" +
				"LONGEST: " + longestStreak + "\n" +
				"HEALTH: " + health + " / " + healthMax + "\n"+
				"SONG: " + songTitle + " from " + songAlbum + " by " + songArtist + "\n" +
				"LENGTH: " + songDuration + "\n" +
				"ELAPSED: " + songElapsed + "\n" +
				"DIFFICULTY: " + songDifficulty.ToString();
		}
	} 
    // Start is called before the first frame update
    void Start()
	{
	    ResetStats();
    }

    // Update is called once per frame
    void Update()
    {
	    UpdateStats();
    }
    #endregion MonoBehavior methods
}
