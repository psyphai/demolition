﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandInteractionManager : MonoBehaviour
{
	public static HandInteractionManager Instance;
	
	public GameObject hand1;
	public GameObject hand2;
	public GameObject[] hands;
	
	public bool IsHandNear(Vector3 positionNeared, float minimumDistance)
	{
		foreach(GameObject hand in hands)
		{
			float distance = Vector3.Distance(hand.transform.position, positionNeared);
			if (distance < minimumDistance)
			{
				return true;
			} 
		}
		return false;
	}
	
	public float HandDistance(Vector3 positionNeared, float maximumDistance)
	{
		float shortestDistance = Vector3.Distance(Camera.main.transform.position,positionNeared);
		foreach(GameObject hand in hands)
		{
			float distance = Vector3.Distance(hand.transform.position, positionNeared);
			if (distance < shortestDistance)
			{
				shortestDistance = distance;
			} 
		}
		return shortestDistance;
	}
	
	
	void Awake()
	{
		Instance=this;
	}
	
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    	    
    }
}
