﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchTarget : MonoBehaviour
{
	public Renderer rendererToHide;
	public GameObject GOToHide;
	public GameObject RagdollToShow;
	private float punchForceMultiplier = 150f;
	public void PunchMessage()
	{
		if(rendererToHide!=null)
		{
			rendererToHide.enabled = false;
		}
		if (GOToHide!=null)
		{
			GOToHide.SetActive(false);
			if (RagdollToShow!=null)
			{
				RagdollToShow.SetActive(true);
				Rigidbody[] rigidbodies = RagdollToShow.GetComponentsInChildren<Rigidbody>();
				foreach(Rigidbody ragRigid in rigidbodies)
				{
					ragRigid.AddForce(Vector3.forward*punchForceMultiplier);
				}
			}
		}
	}
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
