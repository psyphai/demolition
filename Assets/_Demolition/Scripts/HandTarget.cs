﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;
public class HandTarget : MonoBehaviour
{
	public bool withinRange = false;
	public bool timingStarted = false;
	public bool hit = false;
	public int hits = 0;
	public int hitsNeeded = 1;
	public float hitRatio = 0f;
	public bool cleared = false;
	public bool missed = false;
	
	public float minimumHandDistance = 0.5f;
	public float maximumHandDistance = 1f;
	public float timeDuration = 1f;
	
	public bool isHandNearTriggers = true;
	public GameObject messageTarget;
	public string messageName;
	public string messageParameter;
	public GameObject goChangingSize;
	public List<Image> images;
	public Color colorStart = Color.white;
	public Color colorTimeStart = Color.red;
	public Color colorTimeEnd = Color.yellow;
	public Color colorHit = Color.magenta;
	public Color colorMiss = Color.cyan;
	public Color colorClear = Color.blue;
	public bool changeColorStart = true;
	
	public bool changeSizeTiming = true;
	public float timeStart;
	public float timeEnd;
	public float timeElapsed;
	public float timeRatio;
	public Transform changeSizeTimingTransform;
	
	public float maxScale = 1f;
	public float minScale = 0.3f;
	public float scale = 1f;
	public Vector3 scaleVector = new Vector3();
	
	public AudioClip timeClip;
	public AudioClip hitClip;
	public AudioClip clearClip;
	public AudioClip missClip;
	
	public void ColorChange(Color color)
	{
		foreach (Image image in images)
		{
			image.color = color;
		}
	}
	
	public void Hit()
	{
		hits++;
		hitRatio = hits/hitsNeeded;
		if (hitsNeeded>1)
		{
			ColorChange(Color.Lerp(colorHit,colorClear,hitRatio));
		}
		else
		{
			ColorChange(colorHit);
		}
		if (hits >= hitsNeeded)
		{
			Clear();
		}
	}
	
	public void Clear()
	{
		cleared = true;
		ColorChange(colorClear);
		if (messageTarget != null)
		{
			if (messageName != "")
			{
				if (messageParameter == null)
				{
					messageTarget.SendMessage(messageName);
				}
				else
				{
					messageTarget.SendMessage(messageName, messageParameter);
				}
			}
		}
	}
	
	void Miss()
	{
		missed=true;
		
	}
    // Start is called before the first frame update
    void Start()
	{
		RythmverseGameManager.Instance.Register();
	    if (changeColorStart) ColorChange(colorStart);
    }

    // Update is called once per frame
    void Update()
	{
		float distance = HandInteractionManager.Instance.HandDistance(transform.position, maximumHandDistance);
		if (distance < maximumHandDistance)
		{
			if (!timingStarted)
			{
				timingStarted=true;
				ColorChange(colorTimeStart);
			}
			else
			{
				timeElapsed = Time.time - timeStart;
				timeRatio = timeElapsed/timeDuration;
				if (timeElapsed < timeDuration)
				{
					ColorChange(Color.Lerp(colorTimeStart,colorTimeEnd,timeRatio));
				}
				else
				{
					if (!cleared)
					{
						
					}
				}
			}
		}
		timeRatio = timeElapsed / timeDuration;
		if (changeSizeTiming)
		{
			scale = Mathf.Lerp(maxScale,minScale,timeRatio);
			changeSizeTimingTransform.localScale = new Vector3(scale,scale,scale);
		}
	    if (isHandNearTriggers && HandInteractionManager.Instance.IsHandNear(transform.position, minimumHandDistance))
		{
		    Hit();
		}
    }
}
