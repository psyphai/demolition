﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class HoloBanner : MonoBehaviour
{

    public enum Banner
    {
        Barrier01,
        Barrier02,
        Quarantine01,
        Quarantine02,
        Quarantine03,
        Quarantine04,
        Caution01,
        Caution02,
        UnderConstruction,
        DoNotCross,
        Stop,
        DoNotEnter,
        Warning,
        Police,
        PoliceLine,
        CrimeScene
    }    

    [Header("Banner Settings")]
    public Banner banner;
    [Range(-25, 25)]
    public float bannerSpeed = 0;
    public Color bannerColor;

    [Header("Stands Settings")]
    public bool leftStand = true;
    public bool rightStand = true;
    public Color standColor;
    [Range(0, 10)]
    public float blinksPerSecond = 0f;
    [Range(0, 5)]
    public float blinkIntensity = 2f;

    [Header("Do not change")]
    public List<Texture2D> banners = new List<Texture2D>();

    GameObject banner_obj, standLeftMain, standLeftTop, standLeftBottom, standRightMain, standRightTop, standRightBottom;
    GameObject leftGroup, rightGroup;

    Banner oldFrame;

    float phase;
    void Start()
    {
        //Get references to all parts of prefab
        banner_obj = transform.Find("HoloBanner/holo_frame").gameObject;

        leftGroup = transform.Find("StandLeft").gameObject;
        standLeftMain = transform.Find("StandLeft/Main").gameObject;
        standLeftTop = transform.Find("StandLeft/Top").gameObject;
        standLeftBottom = transform.Find("StandLeft/Bottom").gameObject;

        rightGroup = transform.Find("StandRight").gameObject;
        standRightMain = transform.Find("StandRight/Main").gameObject;
        standRightTop = transform.Find("StandRight/Top").gameObject;
        standRightBottom = transform.Find("StandRight/Bottom").gameObject;

        phase = Time.time;

        oldFrame = banner;
        switch (banner)
        {
            case Banner.Barrier01:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[4]);
                break;
            case Banner.Barrier02:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[15]);
                break;
            case Banner.Quarantine01:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[0]);
                break;
            case Banner.Quarantine02:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[1]);
                break;
            case Banner.Quarantine03:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[2]);
                break;
            case Banner.Quarantine04:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[3]);
                break;
            case Banner.Caution01:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[5]);
                break;
            case Banner.Caution02:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[6]);
                break;
            case Banner.UnderConstruction:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[8]);
                break;
            case Banner.DoNotCross:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[7]);
                break;
            case Banner.Stop:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[12]);
                break;
            case Banner.DoNotEnter:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[14]);
                break;
            case Banner.Warning:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[13]);
                break;
            case Banner.Police:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[9]);
                break;
            case Banner.PoliceLine:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[10]);
                break;
            case Banner.CrimeScene:
                banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[11]);
                break;
        }


    }

    // Update is called once per frame
    void Update()
    {
        float amp = blinkIntensity * (1 + Mathf.Cos(blinksPerSecond * (Time.time - phase) * 2 * Mathf.PI)) / 2;

        banner_obj.GetComponent<Renderer>().material.SetColor("_Color", bannerColor);
        banner_obj.GetComponent<Renderer>().material.SetFloat("_HScrollSpeed", bannerSpeed);

        
        if (oldFrame != banner)
        {
            oldFrame = banner;
            switch (banner)
            {
                case Banner.Barrier01:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[4]);
                    break;
                case Banner.Barrier02:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[15]);
                    break;
                case Banner.Quarantine01:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[0]);
                    break;
                case Banner.Quarantine02:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[1]);
                    break;
                case Banner.Quarantine03:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[2]);
                    break;
                case Banner.Quarantine04:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[3]);
                    break;
                case Banner.Caution01:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[5]);
                    break;
                case Banner.Caution02:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[6]);
                    break;
                case Banner.UnderConstruction:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[8]);
                    break;
                case Banner.DoNotCross:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[7]);
                    break;
                case Banner.Stop:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[12]);
                    break;
                case Banner.DoNotEnter:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[14]);
                    break;
                case Banner.Warning:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[13]);
                    break;
                case Banner.Police:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[9]);
                    break;
                case Banner.PoliceLine:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[10]);
                    break;
                case Banner.CrimeScene:
                    banner_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", banners[11]);
                    break;
            }
        }

        if (leftStand)
        {
            if (!leftGroup.activeSelf)
                leftGroup.SetActive(true);
            standLeftMain.GetComponent<Renderer>().material.SetColor("_EmissionColor", bannerColor);
            standLeftTop.GetComponent<Renderer>().material.SetColor("_EmissionColor", standColor * amp);
            standLeftBottom.GetComponent<Renderer>().material.SetColor("_EmissionColor", standColor * amp);
        }
        else if (leftGroup.activeSelf)
            leftGroup.SetActive(false);

        if (rightStand)
        {
            if (!rightGroup.activeSelf)
                rightGroup.SetActive(true);
            standRightMain.GetComponent<Renderer>().material.SetColor("_EmissionColor", bannerColor);
            standRightTop.GetComponent<Renderer>().material.SetColor("_EmissionColor", standColor * amp);
            standRightBottom.GetComponent<Renderer>().material.SetColor("_EmissionColor", standColor * amp);
        }
        else if (rightGroup.activeSelf)
            rightGroup.SetActive(false);
    }
}
