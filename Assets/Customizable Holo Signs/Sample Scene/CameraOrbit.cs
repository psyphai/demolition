﻿using UnityEngine;
using System.Collections;

public class CameraOrbit : MonoBehaviour
{

    public float xRot = 0f;
    public float yRot = 0f;

    public float speed = 5;
    public float distance = 5f;
    public float sensitivity = 1000f;
    public Transform target;
    public Vector3 offset = Vector3.zero;

    void Update()
    {
        yRot += speed * sensitivity * Time.deltaTime;                

        transform.position = offset + target.position + Quaternion.Euler(xRot, yRot, 0f) * (distance * -Vector3.back);
        transform.LookAt(target.position, Vector3.up);
    }
}