﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateIt2 : MonoBehaviour
{
    [SerializeField]
    Gradient gradient1;
    [SerializeField]
    Gradient gradient2;
    [SerializeField]
    Gradient gradient3;
    public float changeSign = 1;
    public float changeFrame = 1.5f;
    public HoloSign hs;


    public float duration = 10;
    float t = 0;

    float startTimeSign, startTimeFrame;
    int deltaSign = 0;
    int totalSign = 24;
    int deltaFrame = 0;
    int totalFrame = 12;
    // Start is called before the first frame update
    void Start()
    {
        startTimeSign = Time.time;
        startTimeFrame = Time.time;
        hs.frame = HoloSign.Frame.Reticle;
        hs.frameSpeed = 0;
        hs.sign = HoloSign.Sign.NoSign;
    }

    // Update is called once per frame
    void Update()
    {
        float value = t % 3;
        t += Time.deltaTime / duration;

        Color colorSign, colorFrame;
        if (value <= 1)
        {
            colorSign = gradient1.Evaluate(value);
            colorFrame = gradient3.Evaluate(1 - value);
        }
        else if (value <= 2)
        {
            colorSign = gradient2.Evaluate(value - 1);
            colorFrame = gradient2.Evaluate(2 - value);
        }
        else
        {
            colorSign = gradient3.Evaluate(value - 2);
            colorFrame = gradient1.Evaluate(3 - value);
        }

        hs.signColor = colorSign;
        hs.frameColor = colorFrame;

        if (Time.time - startTimeSign > changeSign)
        {
            startTimeSign = Time.time;
            deltaSign++;
            deltaSign %= totalSign;
            hs.sign = (HoloSign.Sign)deltaSign;
        }

        if (Time.time - startTimeFrame > changeFrame)
        {

            startTimeFrame = Time.time;
            deltaFrame++;
            deltaFrame %= totalFrame;
            hs.frame = (HoloSign.Frame)deltaFrame;
            if (deltaFrame > 1 && deltaFrame < 8)
                hs.frameSpeed = 5;
            else
                hs.frameSpeed = 0;
        }
    }
}
