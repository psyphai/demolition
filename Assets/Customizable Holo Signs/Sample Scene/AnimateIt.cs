﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateIt : MonoBehaviour
{
    [SerializeField]
    Gradient gradient1;
    [SerializeField]
    Gradient gradient2;
    [SerializeField]
    Gradient gradient3;
    public float changeSign = 2;
    public List<HoloBanner> banners = new List<HoloBanner>();


    public float duration=10;
    float t = 0;

    float startTimeSign, startTimeFrame;
    int deltaSign = 0;
    int totalSign = 16;
    
    // Start is called before the first frame update
    void Start()
    {
        startTimeSign = Time.time;
        startTimeFrame = Time.time;
        foreach (HoloBanner hb in banners)
        {
            hb.banner = HoloBanner.Banner.Barrier01;
            hb.bannerSpeed = 5;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float value = t % 3;
        t += Time.deltaTime / duration;

        Color colorSign;
        if (value <= 1)
        {
            colorSign = gradient1.Evaluate(value);
        }
        else if (value <= 2)
        {
            colorSign = gradient2.Evaluate(value - 1);
        }
        else
        {
            colorSign = gradient3.Evaluate(value - 2);
        }
        foreach (HoloBanner hb in banners)
            hb.bannerColor = colorSign;

        if (Time.time-startTimeSign > changeSign)
        {
            startTimeSign = Time.time;
            deltaSign++;
            deltaSign %= totalSign;
            foreach (HoloBanner hb in banners)
                hb.banner = (HoloBanner.Banner)deltaSign;
        }       
    }
}
