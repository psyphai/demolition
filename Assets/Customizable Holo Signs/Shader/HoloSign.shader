﻿Shader "HoloSign/Hologram Shader"
{
	Properties
	{
		_Color("Color", Color) = (0, 1, 1, 1)
		_MainTex("Base (RGB)", 2D) = "white" {}
		_AlphaTexture ("Alpha Mask", 2D) = "white" {}
		_Scale ("Alpha Tiling", Float) = 3
		_ScrollSpeed("Alpha scroll Speed", Range(-5, 5.0)) = 1.0
		_GlowIntensity ("Glow Intensity", Range(0.01, 2.0)) = 0.5
	}

	SubShader
	{
		Tags{ "Queue" = "Overlay" "IgnoreProjector" = "True" "Queue" = "Transparent" "RenderType"="Transparent" "PreviewType"="Plane" }

		Pass
		{
			Lighting Off 
			ZWrite On
			Blend SrcAlpha One
			Cull Off

			CGPROGRAM
				
				#pragma vertex vertexFunc
				#pragma fragment fragmentFunc

				#include "UnityCG.cginc"

				struct appdata{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					float3 normal : NORMAL;
				};

				struct v2f{
					float4 position : SV_POSITION;
					float2 uv : TEXCOORD0;
					float3 grabPos : TEXCOORD1;					
					float3 viewDir : TEXCOORD2;
					float3 worldNormal : NORMAL;
				};

				fixed4 _Color, _MainTex_ST;
				sampler2D _MainTex, _AlphaTexture;
				half _Scale, _ScrollSpeed, _GlowIntensity;

				v2f vertexFunc(appdata IN){
					v2f OUT;
					OUT.position = UnityObjectToClipPos(IN.vertex);
					OUT.uv = TRANSFORM_TEX(IN.uv, _MainTex);
					OUT.grabPos = UnityObjectToViewPos(IN.vertex);
					OUT.grabPos.y += _Time * _ScrollSpeed;
					OUT.worldNormal = UnityObjectToWorldNormal(IN.normal);
					OUT.viewDir = normalize(UnityWorldSpaceViewDir(OUT.grabPos.xyz));
					return OUT;
				}

				fixed4 fragmentFunc(v2f IN) : SV_Target{
					float2 gp= IN.uv;
					gp.y += _Time * _ScrollSpeed;

					fixed4 alphaColor = tex2D(_AlphaTexture,  gp  * _Scale);
					fixed4 pixelColor = tex2D (_MainTex, IN.uv);
					pixelColor.w = alphaColor.w * pixelColor.a;
					half rim = 1.0-saturate(dot(IN.viewDir, IN.worldNormal));
					return pixelColor * _Color;// * (rim + _GlowIntensity);
				}
			ENDCG
		}
	}
}