﻿Shader "HoloSign/HoloScroll Shader"
{
	Properties
	{
		_Color("Color", Color) = (0, 1, 1, 1)
		_MainTex("Base (RGB)", 2D) = "white" {}
		_VScrollSpeed("Alpha scroll Speed", Range(-25, 25.0)) = 0.0
		_HScrollSpeed("Alpha scroll Speed", Range(-25, 25.0)) = 1.0
	}

	SubShader
	{
		Tags{ "Queue" = "Overlay" "IgnoreProjector" = "True" "Queue" = "Transparent" "RenderType"="Transparent" "PreviewType"="Plane" }

		Pass
		{
			Lighting Off 
			ZWrite On
			Blend SrcAlpha One
			Cull Off

			CGPROGRAM
				
				#pragma vertex vertexFunc
				#pragma fragment fragmentFunc

				#include "UnityCG.cginc"

				struct appdata{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					float3 normal : NORMAL;
				};

				struct v2f{
					float4 position : SV_POSITION;
					float2 uv : TEXCOORD0;
					float3 worldNormal : NORMAL;
				};

				fixed4 _Color, _MainTex_ST;
				sampler2D _MainTex;
				half _VScrollSpeed, _HScrollSpeed;

				v2f vertexFunc(appdata IN){
					v2f OUT;
					OUT.position = UnityObjectToClipPos(IN.vertex);
					OUT.uv = TRANSFORM_TEX(IN.uv, _MainTex);					
					OUT.worldNormal = UnityObjectToWorldNormal(IN.normal);
					return OUT;
				}

				fixed4 fragmentFunc(v2f IN) : SV_Target{
					IN.uv.y += _Time * _VScrollSpeed;
					IN.uv.x += _Time * _HScrollSpeed;
					fixed4 pixelColor = tex2D (_MainTex, IN.uv);
					//pixelColor.w = alphaColor.w * pixelColor.a;
					return pixelColor * _Color ;
				}
			ENDCG
		}
	}
}