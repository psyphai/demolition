﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class HoloSign : MonoBehaviour
{
    public enum Pattern
    {
        plain,
        lines,
        squares
    }

    public enum Frame
    {
        NoFrame,
        Reticle,
        Barrier01,
        Barrier02,
        Caution01,
        Caution02,
        Danger,
        Quarantine,
        Static01,
        Static02,
        Static03,
        Static04
    }

    public enum Sign
    {
        NoSign,
        BioHazard,
        ElectricShock,
        Flamable,
        DeathDanger,
        Danger,
        Radiation,
        NuclearHazard,
        ChemicalHazard,
        Explosives,
        IonizingHazard,
        StrongMagnet,
        VirusHazard,
        MedicalCenter,
        MedicalKit,
        MedicalStaff,
        EPI_Kit,
        COVID_01,
        COVID_02,
        COVID_03,
        COVID_04,
        CCTV_01,
        CCTV_02,
        CCTV_03,
        CCTV_04
    }

    [Header("Frame Settings")]
    public Frame frame;
    [Range(-25, 25)]
    public float frameSpeed=0;
    public Color frameColor;

    [Header("Sign Settings")]
    public Sign sign;
    public Color signColor;
    public Pattern pattern;
    [Range(-5,5)]
    public float scrollSpeed = 0;    
    [Range(0.01f, 5)]
    public float glowIntensity=1;
    [Range(0.01f, 5)]
    public float patternScale = 1;

    [Header("Stands Settings")]
    public bool leftStand = true;
    public bool rightStand = true;
    public Color standColor;
    [Range(0, 10)]
    public float blinksPerSecond=0f;
    [Range(0, 5)]
    public float blinkIntensity = 2f;

    [Header("Do not change")]    
    public List<Texture2D> signs = new List<Texture2D>();
    public List<Texture2D> patterns = new List<Texture2D>();
    public List<Texture2D> frames = new List<Texture2D>();
    
    GameObject sign_obj, frame_obj, standLeftMain, standLeftTop, standLeftBottom, standRightMain, standRightTop, standRightBottom;
    GameObject leftGroup, rightGroup;

    Pattern oldPattern;
    Frame oldFrame;
    Sign oldSign;

    float phase;
    void Start()
    {
                
        //Get references to all parts of prefab
        sign_obj = transform.Find("HoloSign/holo_sign").gameObject;
        frame_obj = transform.Find("HoloSign/holo_frame").gameObject;

        leftGroup = transform.Find("StandLeft").gameObject;
        standLeftMain = transform.Find("StandLeft/Main").gameObject;
        standLeftTop = transform.Find("StandLeft/Top").gameObject;
        standLeftBottom = transform.Find("StandLeft/Bottom").gameObject;

        rightGroup = transform.Find("StandRight").gameObject;
        standRightMain = transform.Find("StandRight/Main").gameObject;
        standRightTop = transform.Find("StandRight/Top").gameObject;
        standRightBottom = transform.Find("StandRight/Bottom").gameObject;

        phase = Time.time;

        oldPattern = pattern;
        switch(pattern)
        {
            case Pattern.plain:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_AlphaTexture", patterns[0]);
                break;
            case Pattern.lines:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_AlphaTexture", patterns[1]);
                break;
            case Pattern.squares:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_AlphaTexture", patterns[2]);
                break;
        }

        oldFrame = frame;
        switch (frame)
        {
            case Frame.NoFrame:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[10]);
                break;
            case Frame.Reticle:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[11]);
                break;
            case Frame.Barrier01:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[0]);
                break;
            case Frame.Barrier02:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[1]);
                break;
            case Frame.Caution01:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[2]);
                break;
            case Frame.Caution02:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[3]);
                break;
            case Frame.Danger:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[4]);
                break;
            case Frame.Quarantine:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[5]);
                break;
            case Frame.Static01:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[6]);
                break;
            case Frame.Static02:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[7]);
                break;
            case Frame.Static03:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[8]);
                break;
            case Frame.Static04:
                frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[9]);
                break;            
        }

        oldSign = sign;
        switch (sign)
        {
            case Sign.NoSign:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[0]);
                break;
            case Sign.BioHazard:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[1]);
                break;
            case Sign.ElectricShock:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[2]);
                break;
            case Sign.Flamable:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[3]);
                break;
            case Sign.DeathDanger:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[4]);
                break;
            case Sign.Danger:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[5]);
                break;
            case Sign.Radiation:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[6]);
                break;
            case Sign.NuclearHazard:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[7]);
                break;
            case Sign.ChemicalHazard:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[8]);
                break;
            case Sign.VirusHazard:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[9]);
                break;
            case Sign.MedicalCenter:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[10]);
                break;
            case Sign.MedicalKit:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[11]);
                break;
            case Sign.MedicalStaff:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[12]);
                break;
            case Sign.EPI_Kit:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[13]);
                break;
            case Sign.IonizingHazard:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[14]);
                break;
            case Sign.StrongMagnet:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[15]);
                break;
            case Sign.COVID_01:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[16]);
                break;
            case Sign.COVID_02:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[17]);
                break;
            case Sign.COVID_03:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[18]);
                break;
            case Sign.COVID_04:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[19]);
                break;
            case Sign.CCTV_01:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[20]);
                break;
            case Sign.CCTV_02:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[21]);
                break;
            case Sign.CCTV_03:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[22]);
                break;
            case Sign.CCTV_04:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[23]);
                break;
            case Sign.Explosives:
                sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[24]);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float amp= blinkIntensity*(1 + Mathf.Cos(blinksPerSecond*(Time.time-phase)*2*Mathf.PI)) / 2;

        sign_obj.GetComponent<Renderer>().material.SetColor("_Color", signColor);
        frame_obj.GetComponent<Renderer>().material.SetColor("_Color", frameColor);

        sign_obj.GetComponent<Renderer>().material.SetFloat("_ScrollSpeed", scrollSpeed);
        sign_obj.GetComponent<Renderer>().material.SetFloat("_Scale", patternScale);
        sign_obj.GetComponent<Renderer>().material.SetFloat("_GlowIntensity", glowIntensity);

        frame_obj.GetComponent<Renderer>().material.SetFloat("_HScrollSpeed", frameSpeed);

        if (oldSign != sign)
        {
            oldSign = sign;
            switch (sign)
            {
                case Sign.NoSign:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[10]);
                    break;
                case Sign.BioHazard:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[1]);
                    break;
                case Sign.ElectricShock:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[2]);
                    break;
                case Sign.Flamable:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[3]);
                    break;
                case Sign.DeathDanger:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[4]);
                    break;
                case Sign.Danger:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[5]);
                    break;
                case Sign.Radiation:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[6]);
                    break;
                case Sign.NuclearHazard:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[7]);
                    break;
                case Sign.ChemicalHazard:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[8]);
                    break;
                case Sign.VirusHazard:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[9]);
                    break;
                case Sign.MedicalCenter:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[10]);
                    break;
                case Sign.MedicalKit:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[11]);
                    break;
                case Sign.MedicalStaff:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[12]);
                    break;
                case Sign.EPI_Kit:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[13]);
                    break;
                case Sign.IonizingHazard:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[14]);
                    break;
                case Sign.StrongMagnet:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[15]);
                    break;
                case Sign.COVID_01:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[16]);
                    break;
                case Sign.COVID_02:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[17]);
                    break;
                case Sign.COVID_03:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[18]);
                    break;
                case Sign.COVID_04:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[19]);
                    break;
                case Sign.CCTV_01:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[20]);
                    break;
                case Sign.CCTV_02:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[21]);
                    break;
                case Sign.CCTV_03:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[22]);
                    break;
                case Sign.CCTV_04:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[23]);
                    break;
                case Sign.Explosives:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", signs[24]);
                    break;
            }
        }

        if (oldPattern != pattern)
        {
            oldPattern = pattern;
            switch (pattern)
            {
                case Pattern.plain:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_AlphaTexture", patterns[0]);
                    break;
                case Pattern.lines:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_AlphaTexture", patterns[1]);
                    break;
                case Pattern.squares:
                    sign_obj.GetComponent<Renderer>().material.SetTexture("_AlphaTexture", patterns[2]);
                    break;
            }            
        }

        if(oldFrame!=frame)
        {
            oldFrame = frame;
            switch (frame)
            {
                case Frame.NoFrame:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[10]);
                    break;
                case Frame.Reticle:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[11]);
                    break;
                case Frame.Barrier01:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[0]);
                    break;
                case Frame.Barrier02:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[1]);
                    break;
                case Frame.Caution01:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[2]);
                    break;
                case Frame.Caution02:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[3]);
                    break;
                case Frame.Danger:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[4]);
                    break;
                case Frame.Quarantine:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[5]);
                    break;
                case Frame.Static01:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[6]);
                    break;
                case Frame.Static02:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[7]);
                    break;
                case Frame.Static03:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[8]);
                    break;
                case Frame.Static04:
                    frame_obj.GetComponent<Renderer>().material.SetTexture("_MainTex", frames[9]);
                    break;
            }
        }

        if (leftStand)
        {
            if (!leftGroup.activeSelf)
                leftGroup.SetActive(true);
            standLeftMain.GetComponent<Renderer>().material.SetColor("_EmissionColor", frameColor);
            standLeftTop.GetComponent<Renderer>().material.SetColor("_EmissionColor", standColor * amp);
            standLeftBottom.GetComponent<Renderer>().material.SetColor("_EmissionColor", standColor * amp);
        }
        else if (leftGroup.activeSelf)
            leftGroup.SetActive(false);

        if (rightStand)
        {
            if (!rightGroup.activeSelf)
                rightGroup.SetActive(true);
            standRightMain.GetComponent<Renderer>().material.SetColor("_EmissionColor", frameColor);
            standRightTop.GetComponent<Renderer>().material.SetColor("_EmissionColor", standColor * amp);
            standRightBottom.GetComponent<Renderer>().material.SetColor("_EmissionColor", standColor * amp);
        }
        else if (rightGroup.activeSelf)
            rightGroup.SetActive(false);
    }
}
